var FACEMOJI_EXTENSION_ID = chrome.i18n.getMessage("@@extension_id"),
    FACEMOJI_PAGE_ID = "415550801876096",
    FACEMOJI_PAGE_URL = "https://www.facebook.com/facemoji",
    FACEMOJI_REVIEW_URL = "http://facemoji.com/review",
    FACEMOJI_OPTIONS_URL = "chrome-extension://" + FACEMOJI_EXTENSION_ID + "/options.html",
    FACEMOJI_STORE_URL = "chrome-extension://" + FACEMOJI_EXTENSION_ID + "/store.html",
    FACEMOJI_VERSION = "";
chrome.runtime.sendMessage({
    method: "getVersion"
}, function (a) {
    FACEMOJI_VERSION = a
});
var styleAdded = !1,
    addStyles = function () {
        if (!styleAdded) {
            styleAdded = !0;
            var a = document.createElement("link");
            a.rel = "stylesheet";
            a.href = chrome.runtime.getURL("css/stickers.css");
            a.type = "text/css";
            document.querySelector("head").appendChild(a)
        }
    };
Element.prototype.getElementByClassName = function (a) {
    return (a = this.getElementsByClassName(a)) && 0 < a.length ? a[0] : null
};
var insertAtCaret = function (a, b, e) {
    if (document.selection) a.focus(), document.selection.createRange().text = b, a.focus();
    else if (a.selectionStart || 0 === a.selectionStart) {
        var c = a.selectionStart,
            f = a.selectionEnd,
            g = a.scrollTop;
        a.value = a.value.substring(0, c) + b + a.value.substring(f, a.value.length);
        a.focus();
        a.selectionStart = c + b.length;
        a.selectionEnd = c + b.length;
        a.scrollTop = g
    } else a.value += b, a.focus(); if (e) {
        var d = document.createEvent("KeyboardEvent");
        d["undefined" !== typeof d.initKeyboardEvent ? "initKeyboardEvent" :
            "initKeyEvent"]("keyup", !1, !1, window, String.fromCharCode(32), 32, !1, !1, !1, !1, !1);
        setTimeout(function () {
            a.dispatchEvent(d)
        }, 300)
    }
}, Utils = {
        FB_DTSG: "",
        getFbDtsg: function () {
            if (Utils.FB_DTSG) return Utils.FB_DTSG;
            var a = document.getElementsByName("fb_dtsg");
            a.length && (Utils.FB_DTSG = a[0].value);
            return Utils.FB_DTSG
        },
        generateParameters: function (a) {
            var b = [];
            for (key in a) b.push(key, "=", encodeURIComponent(a[key]), "&");
            a = b.join("");
            return a = a.substr(0, a.length - 1)
        },
        sendMessage: function (a) {
            a.fb_dtsg = Utils.getFbDtsg();
            a.send = "Reply";
            a.wwwupp = "V3";
            a.charset_test = "\u20ac,\u00b4,\u20ac,\u00b4,\u6c34,\u0414,\u0404";
            a.tids && (a.tids = a.tids.replace("t_id", "id"));
            var b = new XMLHttpRequest;
            b.open("POST", "https://m.facebook.com/messages/send/?icm=1", !0);
            b.setRequestHeader("content-type", "application/x-www-form-urlencoded");
            b.onload = function (a) {};
            b.send(Utils.generateParameters(a))
        }
    };